<div class="row">
    <div class="col-lg-8 main-chart">
        <?php
        if (isset($actividades)) {
            if (count($actividades) < 0) {
                ?>
                <h1>NO TIENE RECORRIDO REGISTRADO.</h1>
            <?php } else { ?>
                <div id="map" style="width: 100%; height: 100%;"></div>
                <?php
            }
        }
        ?>
    </div>
    <div class="col-lg-4 ds" style="height: 100%;">
        <form method="GET">
            <div class="weather-2 pn">
                <div class="weather-2-header">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6">
                            <p>INFORMACIÓN A MOSTRAR</p>
                        </div>
                        <div class="col-sm-6 col-xs-6 goright">
                            <p class="small"><?php echo strtoupper(strftime("%B %d,%Y")); ?></p>
                        </div>
                    </div>
                </div>

                <div class="row data">
                    <div class="col-sm-6 col-xs-6 goleft">
                        <div class="form-group">
                            <label class="control-label" for="inputInicio">De (Fecha inicio):</label>
                            <input type="date" class="form-control" id="inputInicio" name="fecha_inicio" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="inputFinal" id="labelInputFinal">A (Fecha término):</label>
                            <input class="form-control" type="date" id="inputFinal" name="fecha_fin" required>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-6 goright">
                        <div class="checkbox" style="text-align: right;">
                            <label>
                                <input type="checkbox" onchange="cambia_checkbox()" id="checkFecha"><span class="checkbox-material"></span> Solo fecha inicial.
                            </label>
                        </div>
                    </div>
                </div>
                <div class="centered">
                    <?php if (isset($mensaje)) { ?>
                        <div class="alert alert-dismissable alert-danger" style="display: none;">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <p>La fecha de inicio debe ser mejor que la fecha final. Ejemplo: <br>Fecha inicio: 2014/10/11 <br>Fecha final: 2014/10/20 </p>
                        </div>
                    <?php } ?>
                    <button class="btn btn-primary" type="submit">Buscar</button>
                </div>
            </div>
        </form>
        <br>
        <div class="green-panel pn">
            <div class="green-header">
                <h5>INFORMACIÓN</h5>
            </div>
            <p><img src="<?php echo base_url() ?>dist/img/ui-zac.jpg" class="img-circle" width="50"></p>
            <p><b><?php echo $info_usuario[0]->nombre . ' ' . $info_usuario[0]->apellidos; ?></b></p>
            <p><?php echo $info_usuario[0]->email ?></p>
            <div class="row">
                <div class="col-md-6">
                    <p class="small mt">DISTANCIA RECORRIDA</p>
                    <?php
                    if (isset($actividades)) {
                        if (count($actividades) > 0) {
                            $dist_total = 0;
                            $lat_anterior = $actividades[0]->latitud;
                            $lon_anterior = $actividades[0]->longitud;
                            foreach ($actividades as $key => $value) {
                                if($value->latitud != 0 && $value->latitud != $lat_anterior){
                                   $dist_total += distancia($lat_anterior, $lon_anterior, $value->latitud, $value->longitud);
                                    $lat_anterior = $value->latitud;
                                    $lon_anterior = $value->longitud; 
                                }
                            }
                            echo "<p>" . ($dist_total * 0.001) . " km</p>";
                        } else {
                            echo "<p>-----.--</p>";
                        }
                    } ?>
                </div>
                <div class="col-md-6">
                    <p class="small mt">CALORIAS QUEMADAS</p>
                    <p>---.---</p>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function initMap() {
        var obj = {lat: parseFloat('20.6570281'), lng: parseFloat('-103.3976109')};
<?php if (isset($actividades) && count($actividades) > 0) { ?>
            obj = {lat: parseFloat('<?php echo $actividades[0]->latitud; ?>'), lng: parseFloat('<?php echo $actividades[0]->longitud; ?>')};
<?php } ?>
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: obj,
            mapTypeId: google.maps.MapTypeId.NORMAL
        });

        var flightPlanCoordinates = [];
<?php
if (isset($actividades) && count($actividades) > 0) {
    foreach ($actividades as $key => $value) {
        ?>
                var object = {lat: parseFloat('<?php echo $value->latitud; ?>'), lng: parseFloat('<?php echo $value->longitud; ?>')};
                flightPlanCoordinates.push(object);
                var marker<?php echo $key; ?> = new google.maps.Marker({
                    position: object,
                    map: map,
                    icon: "<?php echo base_url(); ?>dist/img/flag.png",
                    title: "<?php echo $value->fecha; ?>"
                });

                marker<?php echo $key; ?>.addListener('click', function () {
                    new google.maps.InfoWindow({content: "<div id='content'></div><div id='bodyContent'>Fecha evento <?php echo $value->fecha; ?></div>"}).open(map, marker<?php echo $key; ?>);
                });
        <?php
    }
}
?>

        var flightPath = new google.maps.Polyline({
            path: flightPlanCoordinates,
            geodesic: true,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 2
        });

        flightPath.setMap(map);
    }
<?php

function distancia($punto1La, $punto1Lo, $punto2La, $punto2Lo) {
    $_RADIO_TIERRA = 6371000;
    $dLat = deg2rad($punto2La - $punto1La);
    $dLng = deg2rad($punto2Lo - $punto1Lo);
    $a = sin($dLat / 2) * sin($dLat / 2) + cos(deg2rad($punto1La)) * cos(deg2rad($punto2La)) * sin($dLng / 2) * sin($dLng / 2);
    $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
    return (Double) ($_RADIO_TIERRA * $c);
}
?>

</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyYInpmC3f2gz0ZSvgyOl9Dgm2A8hdj-Y&signed_in=true&callback=initMap"></script>