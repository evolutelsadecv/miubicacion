<div style="padding: 10px;">
    <table class="table table-striped table-advance table-hover data-table">
        <thead>
            <tr>
                <td>
                    #
                </td>
                <td>
                    Nombre
                </td>
                <td>
                    Apellido(s)
                </td>
                <td>
                    Fecha de Nacimiento
                </td>
                <td>
                    ZIP
                </td>
                <td>
                    Email
                </td>
                <td>
                    Acciones
                </td>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($usuarios as $key => $value) {
                ?>
                <tr>
                    <td><?php echo $key + 1 ?></td>
                    <td><?php echo $value->nombre ?></td>
                    <td><?php echo $value->apellidos ?></td>
                    <td>
                        <?php 
                            $transf = strtotime($value->fecha_nacimiento);
                            echo strftime("%d de %B de %Y", $transf);
                        ?>
                    </td>
                    <td><?php echo $value->zip ?></td>
                    <td><?php echo $value->email ?></td>
                    <td>
                        <a class="btn btn-primary btn-xs" href="<?php echo base_url() ?>index.php/Usuario/recorrido/<?php echo $value->id_usuario ?>"><i class="fa fa-map"></i></a>
                        <button class="btn btn-danger btn-xs" onclick="eliminar_usuario('<?php echo $value->id_usuario ?>','<?php echo $value->nombre ?>')"><i class="fa fa-trash-o "></i></button>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
</div>