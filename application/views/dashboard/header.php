<html>
    <header>
        <title><?= $title; ?></title>
        <link href="<?= base_url() ?>dist/img/logo.png" rel="icon">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Dashboard">
        <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
        <link href="<?php echo base_url() ?>dist/css/bootstrap.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>dist/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href="<?php echo base_url() ?>dist/css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>dist/css/zabuto_calendar.css" rel="stylesheet" type="text/css" />  
        <link href="<?php echo base_url() ?>dist/css/style.css" rel="stylesheet" />
        <link href="<?php echo base_url() ?>dist/css/style-responsive.css" rel="stylesheet" />
    </header>
    <body>
        <?php
            date_default_timezone_set("America/Mexico_City");
            setlocale(LC_ALL, 'es_ES.UTF-8');
        ?>
        <section id="container" >
            <header class="header black-bg">
                <div class="sidebar-toggle-box">
                    <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Menú"></div>
                </div>
                <a href="<?php echo base_url() ?>index.php/Dashboard" class="logo"><b>Evolutel</b></a>
                <div class="nav notify-row" id="top_menu"></div>
                <div class="top-menu">
                    <ul class="nav pull-right top-menu">
                        <li><a class="logout" href="<?php echo base_url() ?>index.php/Login/logout">Logout</a></li>
                    </ul>
                </div>
            </header>
            <aside>
                <div id="sidebar"  class="nav-collapse ">
                    <ul class="sidebar-menu" id="nav-accordion">
                        <p class="centered"><a href="profile.html"><img src="<?php echo base_url() ?>dist/img/ui-sam.jpg" class="img-circle" width="60"></a></p>
                        <h5 class="centered"><?php echo $nombre.' '.$apellidos ?></h5>
                        <li class="mt">
                            <a class="active" href="<?php echo base_url() ?>index.php/Dashboard">
                                <i class="fa fa-dashboard"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>
                        <li class="sub-menu">
                            <a href="javascript:;" >
                                <i class="fa fa-desktop"></i>
                                <span>Usuarios</span>
                            </a>
                            <ul class="sub">
                                <li><a  href="<?php echo base_url() ?>index.php/Usuario">Lista</a></li>
                            </ul>
                        </li>
                        <li class="sub-menu">
                            <a  href="<?php echo base_url() ?>index.php/Actividad">
                                <i class="fa fa-line-chart"></i>
                                <span>Actividad</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </aside>
            <section id="main-content">
                <section class="wrapper">

