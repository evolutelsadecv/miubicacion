</section>
</section>
</section>
<script src="<?php echo base_url() ?>dist/js/jquery.js"></script>
<script src="<?php echo base_url() ?>dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>dist/js/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/jquery.dcjqaccordion.2.7.js" class="include" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url() ?>dist/js/zabuto_calendar.js"></script>	 
<script src="<?php echo base_url() ?>dist/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>dist/js/common-scripts.js"></script>
<script>
    $(document).ready(function () {
        $("#my-calendar").zabuto_calendar({
            action: function () {
                return myDateFunction(this.id, false);
            },
            action_nav: function () {
                return myNavFunction(this.id);
            },
            legend: [
                {type: "text", label: "", badge: "00"},
                {type: "block", label: "", }
            ]
        });

        $('.data-table').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [10, 25, 50, 75, 100],
            "columnDefs": [
                {"orderable": false, "targets": 6}
            ],
            "language": {
                "lengthMenu": "Número registros _MENU_",
                "zeroRecords": "Sin datos",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(Total de registros _MAX_)",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": "Siguiente",
                    "previous": "Previo"
                }
            }
        });
    });


    function myNavFunction(id) {
        $("#date-popover").hide();
        var nav = $("#" + id).data("navigation");
        var to = $("#" + id).data("to");
        console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
    }

    $(function () {
        $('select.styled').customSelect();
    });

    function eliminar_usuario(id, nombre) {
        var r = confirm("Esta seguro que desea eliminarlo a " + nombre + "?");
        if (r == true) {
            location.href = "<?= base_url(); ?>index.php/Usuario/delete/" + id;
        }
    }

    function cambia_checkbox() {
        if ($('#checkFecha').get(0).checked) {
            $('#inputFinal').attr('disabled', 'disabled');
            $('#inputFinal').removeAttr('required');
            $('#labelInputFinal').attr('style', 'color:gray;');
            $('#inputFinal').val('');
        } else {
            $('#labelInputFinal').attr('style', 'color:black;');
            $('#inputFinal').removeAttr('disabled');
            $('#inputFinal').attr('required', '');
        }
    }
</script>
</body>
</html>
