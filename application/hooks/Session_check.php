<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Session_check
 *
 * @author Evolutel
 */
class Session_check extends CI_Hooks {

    private $ci;
    private $output;
    private $uriOpens = array("login" => true,"datos" => true,"" => true);
    private $permisos = array(
        "1" => array("dashboard" => true,"usuario" => true,"actividad" => true),
    );

    public function __construct() {
        $this->ci = &get_instance();
    }

    public function check_login() {
        !$this->ci->load->library('session') ? $this->ci->load->library('session') : false;
        !$this->ci->load->helper('url') ? $this->ci->load->helper('url') : false;
        if ($this->ci->session->userdata('id_admin') == false && !isset($this->uriOpens[strtolower($this->ci->uri->segment(1))])) {
            redirect(site_url('login'));
        }
    }


    public function check_permission() {
        !$this->ci->load->library('session') ? $this->ci->load->library('session') : false;
        !$this->ci->load->helper('url') ? $this->ci->load->helper('url') : false;
        $id_rol = $this->ci->session->userdata('id_rol');
        if (!isset($this->uriOpens[strtolower($this->ci->uri->segment(1))])) {
            if (!isset($this->permisos[$id_rol][strtolower($this->ci->uri->segment(1))])) {
                show_error("No cuentas con los permisos suficientes para acceder al contenido ", 403, "Permiso denegado");
            }
        } else {
            return;
        }
    }

}
