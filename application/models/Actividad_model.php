<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 *
 * @author Rosita
 */
class Actividad_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('encrypt');
    }

    public function getActividad($id, $fecha_i, $fecha_t) {
        $id = $this->decrypt($id);
        $sql = "SELECT latitud,longitud,DATE_FORMAT(fecha, '%d-%m-%Y %h:%i %p')as fecha "
                . "FROM actividad "
                . "WHERE id_usuario = $id AND (DATE_FORMAT(fecha, '%Y-%m-%d') BETWEEN '$fecha_t' AND '$fecha_i');";

        $result = $this->db->query($sql);
        return $result->result();
    }

    public function insertActividad($data) {
        try {
            $data['id_usuario'] = (int) $data['id_usuario'];
            return $this->db->insert('actividad', $data);
        } catch (Exception $e) {
            return false;
        }
    }

    private function decrypt($q) {
        $dirty = array("+", "/", "=");
        $clean = array("_P_", "_S_", "_E_");
        $q = str_replace($clean, $dirty, $q);
        $cryptKey = $this->session->userdata('encryption');
        $qDecoded = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), base64_decode($q), MCRYPT_MODE_CBC, md5(md5($cryptKey))), "\0");
        return( $qDecoded );
    }

    private function encrypt($q) {
        $cryptKey = $this->session->userdata('encryption');
        $dirty = array("+", "/", "=");
        $clean = array("_P_", "_S_", "_E_");
        $qEncoded = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), $q, MCRYPT_MODE_CBC, md5(md5($cryptKey))));
        return str_replace($dirty, $clean, $qEncoded);
    }

}
