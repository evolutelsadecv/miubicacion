<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Usuario_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('encrypt');
    }

    public function existeUsuario($email) {
        $sql = "SELECT * FROM usuario WHERE UPPER(email) = UPPER('$email')";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function validar_usuario($email, $contrasenia) {
        $contrasenia = sha1(md5($contrasenia));
        $sql = "SELECT id_usuario,nombre,apellidos,email "
                . "FROM usuario "
                . "WHERE UPPER(email) = UPPER('$email') AND password = '$contrasenia' "
                . "LIMIT 1";
        $datos = $this->db->query($sql);
        $datos = $datos->row();
        if(count($datos)> 0){
            $datos->success = 2;
            return $datos;
        }
        return false;
    }

    public function getUsuarios() {
        $sql = "SELECT * FROM usuario WHERE estatus = 1";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            foreach ($result->result() as $row) {
                $row->id_usuario = $this->encrypt($row->id_usuario);
                $row->password = $this->encrypt($row->password);
            }
        }
        return $result->result();
    }

    public function deleteUsuario($id) {
        $id = $this->decrypt($id);
        $data = array('estatus' => 0);
        $this->db->where('id_usuario', $id);
        $this->db->update('usuario', $data);
    }

    public function getUsuario($id) {
        $id = $this->decrypt($id);
        $sql = "SELECT * FROM usuario WHERE id_usuario = $id AND estatus = 1";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            foreach ($result->result() as $row) {
                $row->id_usuario = $this->encrypt($row->id_usuario);
            }
        }
        return $result->result();
    }

    public function insertUsuario($data) {
        try {
            $result = $this->db->query("SELECT * FROM usuario WHERE UPPER(email) = UPPER('" . $data['email'] . "')");
            if ($result->num_rows() > 0) {
                return false;
            }
            else{
                $this->db->trans_start();
                $data['password'] = sha1(md5($data['password']));
                $this->db->insert('usuario', $data);
                $id = $this->db->insert_id();
                $this->db->trans_complete();
                return $id;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    private function decrypt($q) {
        $dirty = array("+", "/", "=");
        $clean = array("_P_", "_S_", "_E_");
        $q = str_replace($clean, $dirty, $q);
        $cryptKey = $this->session->userdata('encryption');
        $qDecoded = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), base64_decode($q), MCRYPT_MODE_CBC, md5(md5($cryptKey))), "\0");
        return( $qDecoded );
    }

    private function encrypt($q) {
        $cryptKey = $this->session->userdata('encryption');
        $dirty = array("+", "/", "=");
        $clean = array("_P_", "_S_", "_E_");
        $qEncoded = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), $q, MCRYPT_MODE_CBC, md5(md5($cryptKey))));
        return str_replace($dirty, $clean, $qEncoded);
    }

}
