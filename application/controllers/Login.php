<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('Login_model');
    }

    public function index() {
        if ($_POST) {
            $usuario = $this->input->post('email');
            $contrasenia = $this->input->post('password');
            $resultado = $this->Login_model->validar_usuario($usuario, $contrasenia);
            if (count($resultado) > 0) {
                $newdata = array(
                    'nombre' => $resultado->nombre,
                    'apellidos' => $resultado->apellidos,
                    'correo'=>$resultado->email,
                    'id_admin' => $resultado->id_admin,
                    'id_rol' => $resultado->id_rol,
                    'encryption' => $this->generarCadena()
                );
                $this->session->set_userdata($newdata);
                switch ($resultado->id_rol) {
                    case 1:
                        redirect('Dashboard','refresh');
                        break;
                }
            } else {
                $data['error'] = "Usuario o contraseña incorrecta, por favor vuelva a intentar";
            }
        }
        $data["title"] = ':: MI UBICACIÓN ::';
        $this->load->view('login/index', $data);
    }
    function logout() {
        session_destroy();
        $this->session->userdata = array();
        redirect('login','refresh');
    }
    
    private function generarCadena() {
        $caracteres = "abcdefghijklmnopqrstuvwxyz1234567890";
        $numerodeletras = 56;
        $cadena = "";
        for ($i = 0; $i < $numerodeletras; $i++) {
            $cadena = $cadena . substr($caracteres, rand(0, strlen($caracteres)), 1);
        }
        return $cadena;
    }

}