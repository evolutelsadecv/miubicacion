<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
defined('BASEPATH') OR exit('No direct script access allowed');


class Actividad extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
    }
    public function index(){
        $data["title"] = ':: Dashboard ::';
        $data["nombre"] = $this->session->userdata('nombre');
        $data["apellidos"] = $this->session->userdata('apellidos');
        $data["correo"] = $this->session->userdata('correo');
        $this->load->view('dashboard/header', $data);
        $this->load->view('dashboard/actividad/index');
        $this->load->view('dashboard/footer');
    }
}