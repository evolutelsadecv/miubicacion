<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
defined('BASEPATH') OR exit('No direct script access allowed');


class Usuario extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('Usuario_model');
        $this->load->model('Actividad_model');
    }
    
    public function index(){
        $data["title"] = ':: Usuario ::';
        $data["nombre"] = $this->session->userdata('nombre');
        $data["apellidos"] = $this->session->userdata('apellidos');
        $data["correo"] = $this->session->userdata('correo');
        $data["usuarios"] = $this->Usuario_model->getUsuarios();
        $this->load->view('dashboard/header', $data);
        $this->load->view('dashboard/usuario/index',$data);
        $this->load->view('dashboard/footer');
    }
    
    public function recorrido($id){
        $data["title"] = ':: Recorrido ::';
        $data['info_usuario'] = $this->Usuario_model->getUsuario($id);
        $data["nombre"] = $this->session->userdata('nombre');
        $data["apellidos"] = $this->session->userdata('apellidos');
        $data["correo"] = $this->session->userdata('correo');
        if($_GET){
            $data['fecha_inicio'] = $this->input->get('fecha_inicio');
            $data['fecha_fin'] = $this->input->get('fecha_fin');
            $fi = $data['fecha_inicio'];
            $ft = $data['fecha_fin'];
            if ($ft == null || $ft == '' || $ft == ' ') {
                $ft = $fi;
            }
            $data["actividades"] = $this->Actividad_model->getActividad($id,$ft,$fi);
        }
        $this->load->view('dashboard/header', $data);
        $this->load->view('dashboard/usuario/recorrido',$data);
        $this->load->view('dashboard/footer');
    }
    
    public function delete($id_usuario) {
        $this->Usuario_model->deleteUsuario($id_usuario);
        redirect('Usuario', 'refresh');
    }
}