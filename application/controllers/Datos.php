<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Datos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('Usuario_model');
        $this->load->model('Actividad_model');
    }

    public function existeEmail($email) {
        $result = $this->Usuario_model->existeUsuario($email);
        header('Content-Type: application/json');
        http_response_code(202);
        echo json_encode(array("existe" => $result));
        exit;
    }

    public function login() {
        if ($_POST) {
            $resultado = $this->Usuario_model->validar_usuario($this->input->post("email"), $this->input->post("password"));
            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode($resultado);
                exit;
            } else {
                header('Content-Type: application/json');
                http_response_code(404);
                echo json_encode(array("success" => 1, "message" => "Usuario invalido"));
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 0, "mensaje" => "Ha ocurrido un error."));
        exit;
    }

    public function registrarUsuario() {
        if ($_POST) {
            $registro = array(
                "nombre" => $this->input->post("nombre"),
                "apellidos" => $this->input->post("apellidos"),
                "email" => $this->input->post("email"),
                "password" => $this->input->post("password"),
                "zip" => $this->input->post("zip"),
                "fecha_nacimiento" => $this->input->post("fecha_nacimiento"),
                "estatus" => 1,
            );
            $resultado = $this->Usuario_model->insertUsuario($registro);
            if ($resultado != false) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 2, "id_usuario" => $resultado));
                exit;
            } else {
                header('Content-Type: application/json');
                http_response_code(404);
                echo json_encode(array("success" => 1, "message" => "Correo electrónico ya existe"));
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 0, "message" => "Ha ocurrido un error."));
        exit;
    }

    public function registrarActividad() {
        if ($_POST) {
            $registro = array(
                "id_usuario" => (int)$this->input->post("id_usuario"),
                "latitud" => $this->input->post("latitud"),
                "longitud" => $this->input->post("longitud"),
                "fecha" => $this->input->post("fecha")
            );

            if ($this->Actividad_model->insertActividad($registro)) {
                header('Content-Type: application/json');
                http_response_code(202);
                echo json_encode(array("success" => 1,"message"=> "Registro exitoso"));
                exit;
            }
        }
        header('Content-Type: application/json');
        http_response_code(404);
        echo json_encode(array("success" => 0,"message" => "Ha ocurrido un error."));
        exit;
    }

}
